<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Registration Form</title>
        <link href="style.css" rel="stylesheet" type="text/css"/>
    </head>
    
    
    <body class="body">
            <div class="header">
                <hr/>
                <?php 
                    date_default_timezone_set("Asia/Dhaka");
                ?>
                <span class="float"><?php echo date("d/m/Y");?></span><br/>
                <span class="float"><?php echo date("l");?></span><br/>

                <span class="float"><?php echo date("h:iA");?></span><br/>
                <div class="search_form">
                        <form method="get" action="search.php">
                                <input type="search" name="search" placeholder="search keyword..."/>
                                <input type="submit" name="submit" value="search"/>
                        </form>
                </div>
                
                <hr/>
            </div>
    </body>
</html>

