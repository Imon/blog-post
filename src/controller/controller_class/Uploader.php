<?php


namespace App\controller\controller_class;


class Uploader {
    
    static public function upload($files){
        $tmp_name           = $files['tmp_name'];
        $destinaion_folder  = $_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."PhpProject1".DIRECTORY_SEPARATOR."image".DIRECTORY_SEPARATOR;
        $destinaion         = self::uniqname($files['name']);
        $query              = move_uploaded_file($tmp_name, $destinaion_folder.$destinaion);
    }
    
    static private function uniqname($filename){
        $filename_parts = explode(".", $filename);
        $ext            = array_pop($filename_parts);
        $name           = implode(".", $filename_parts);
        
        return $name."_".time().".".$ext;
    }
}
