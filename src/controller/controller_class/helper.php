<?php

namespace App\controller\controller_class;

class helper { 
    public function dateFormat($date){
        return date('F j, Y, g:i A', strtotime($date));
    }
    public function textShorten($text, $limit = 400){
        $text = $text." ";
        $text = substr($text, 0, $limit);
        $text = $text."....";
        return $text;
//        $text = $text." ";
//        $text = substr($text, 0, $limit);
//        $text = $text."....";
//        return $text;
    }
}
