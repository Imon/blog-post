<?php
session_start();
use App\controller\controller_class\details;
include_once ($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."PhpProject1".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");

   // include_once './src/controller/controller_class/details.php';
    $users = new details();
    if($users->getSession()){
        header("location: views/all_files/details/show.php");
    }
   
?>



<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Registration Form</title>
        <link href="style.css" rel="stylesheet" type="text/css"/>
    </head>
    
    
    <body class="body">
        <?php include("header.php");?>
        <div class="area">
            <button style="float: right;"><h1><a style="text-decoration: none;" href="views/all_files/details/login.php">Login</a></h1></button>
            
            <p style="Color: RED;  float: left; margin-left: 100px">* Required field.</p>
            <br/><br/>
            
            <form action="views/all_files/details/store.php" method="post" enctype="multipart/form-data">
                <table class="table leftalign">
                    <tr>
                        <td>First Name</td>
                        <td><input type="text" name="first_name" required>*</td>
                    </tr>
                    <tr>
                        <td>Last Name</td>
                        <td><input type="text" name="last_name" required>*</td>
                    </tr>
                    <tr>
                        <td>Father Name</td>
                        <td><input type="text" name="father_name" required>*</td>
                    </tr>
                    <tr>
                        <td>Mother Name</td>
                        <td><input type="text" name="mother_name" required>*</td>
                    </tr>
                    <tr>
                        <td>Email Address</td>
                        <td><input type="text" name="email" required>*</td>
                    </tr>
                    <tr>
                        <td>Date of Birth</td>
                        <td><input type="date" name="d_o_birth" required>*</td>
                    </tr>
                    <tr>
                        <td>Profile Picture</td>
                        <td><input type="file" name="image"></td>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td><input type="password" name="password" required>*</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" name="submit" value="submit" /></td>
                    </tr>
                </table>
            </form>
        </div>
        
        <div>
            
        </div>
        <?php include("footer.php");?>
        
        
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="js/custom.js"></script>
        <script src="js/parallax.js"></script>
    </body>
</html>
