<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Registration Form</title>
        <link href="style.css" rel="stylesheet" type="text/css"/>
    </head>
    
    
    <body class="body">
        <div class="">
            <div class="footer">
                
                <hr/>
                <p>
                    &copy; <?php echo date("Y");?> xtrasell ltd.
                </p>
                <h3><a href="http://xtrasell.com/">www.xtrasell.com</a></h3>
                <hr/>
                
            </div>
        </div>
    </body>
</html>


