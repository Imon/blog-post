<?php

session_start();
use App\controller\controller_class\Admin;
use App\controller\controller_class\blog;
use App\controller\controller_class\details;
use App\controller\controller_class\helper;

include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."PhpProject1".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");

$all_posts_class = new blog;

$admin = new Admin();
$admin->getSession();
    $username = $_SESSION['admin_name'];


$helper = new helper();
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Admin's Blog</title>
        <link href="../../../style.css" rel="stylesheet" type="text/css"/>
    </head>
    
    
    <body>
        
        <!--Header Area-->
        <?php include("../../../header.php");?>
        <!--end header area-->
        
        <div class="area">
            <div class="post_body">
                <h2 style="text-align: center; "><?php echo "Hello,"." ";?><a style="text-decoration: none; color: #fff;" href=""><?php echo $username; ?></a> | <a style="text-decoration: none; color: #fff;" href="admin_logout.php">Logout</a> | <a style="text-decoration: none; color: #fff;" href="add_post.php">Add New</a>|<a style="text-decoration: none; color: #fff;" href="../../../calculator.php">Calculator</a></h2><br><br>
               
                <!--pagination-->
               <?php
                $post_per_page = 3;
                 //$number_of_page = $all_posts_class->pagination();
                 if(isset($_GET['page'])){
                    $page = $_GET['page'];
                 }  else {
                     $page = 1;
                 }
                 $start_from = ($page-1) * $post_per_page;

              
            
            $pagignation = $all_posts_class->pagination();
                $total_page = ceil($pagignation/$post_per_page);
                for($i =1; $i<=$total_page; $i++){
                     echo "<span class='pagination'><a href='all_posts.php?page=$i'>".$i." "."|</a></span>";
                }
                //Pagignation end
                ?>
                <!--end of pagination-->
                
                <button><a href="add_category.php">Add Category</a></button>
                <button><a href="add_post.php">Add Blog</a></button>
                <?php 
                $all_posts = $all_posts_class->select($start_from, $post_per_page);
                    foreach ($all_posts as $blog_posts){
                ?>
                
                <!--all posts-->
                    <hr>
                    <?php echo "<br>"; echo "<br>"; ?>
                    
                                <h1><a style="text-decoration: none; color: #b98966;" href="edit.php?id=<?php echo $blog_posts['id']?>"><?php echo $blog_posts['title'];?></a></h1>
                                <hr>
                                    <?php
                                    echo "Writer : ".$blog_posts['author'].",  ";
                                    echo "Date : ".$helper->dateFormat($blog_posts['date']);
                                    ?>
                                    <hr>
                                    <?php
                                    echo "<br>";
                                    echo $helper->textShorten($blog_posts['body']);
                                    ?>
                                    <a style="text-decoration: none; color: red;" href="edit.php?id=<?php echo $blog_posts['id']?>">Edit</a> | 
                                    <a style="text-decoration: none; color: red;" href="delete.php?id=<?php echo $blog_posts['id']?>">Delete</a>
                                    
                    <?php
                                    echo "<br>";
                                    echo "<br>";
                                    echo "<br>";
                    ?>
                   <hr>

                <?php 
                }
                //End Foreach
                ?>
            </div>
            <?php include("../blog_post/sidebar.php");?>
        </div>
        <!--Footer Area-->
        <?php include("../../../footer.php");?>
        <!--End of Footer area-->
    </body>
</html>
