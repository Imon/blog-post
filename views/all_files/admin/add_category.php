<?php
session_start();
use App\controller\controller_class\Admin;
include($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."PhpProject1".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");

$admin = new Admin();
$logedIn = $admin->getSession();
if(!$logedIn){
    header("location: admin_login.php");
}else{
    $name = $_SESSION['admin_name'];
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Registration Form</title>
        <link href="../../../style.css" rel="stylesheet" type="text/css"/>
    </head>
    
    
    <body class="body">
        <?php include("../../../header.php");?>
        
        <div class="area">
            <?php
                if($_SERVER["REQUEST_METHOD"] == "POST"){
                    $name = $_POST['name'];
                        if(empty($name)){
                            echo "<script>alert('Field can not be empty!!!')</script>";
                    }  else {
                            $admin->category_store($name);
                    } 
                }
            ?>
            <?php echo $name;?>
            <button><a href="all_posts.php">Blog</a></button>
            <button><a href="add_post.php">Add Blog</a></button>
            <button><a href="admin_logout.php">Logout</a></button><br><br>
            
            <form action="" method="post">
                    <table>
                            <tr>
                                <td>Category Name</td>
                                <td><input type="text" name="name"/></td><br><br>
                            </tr>
                            <tr>
                                <td></td>
                                <td><input type="submit" name="submit" value="Submit"/></td>
                            </tr>
                    </table>
            </form>
        </div>
        
        <?php include("../../../footer.php");?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="js/custom.js"></script>
        <script src="js/parallax.js"></script>
    </body>
</html>


