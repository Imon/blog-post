<?php
    use App\controller\controller_class\Admin;
    use App\controller\controller_class\blog;
    use App\controller\controller_class\helper;
       
    include ($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."PhpProject1".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");
    
    $admin = new Admin();
    if($admin->getSession()){
        header("location:all_posts.php");
        die();
    }
        if($_SERVER["REQUEST_METHOD"] == "POST"){
            $name       = $_POST['name'];
            $password   = $_POST['password'];
            if(!empty($name) && !empty($password)){
                $login = $admin->login($name, $password);
                if($login){
                    $admin->getSession();
                    $admin_name = $_SESSION['admin_name'];
                }
            } 
        } 
    
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Your Details</title>
        <link href="../../../style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php include("../../../header.php");?>
        <div class="area">
            
            <button style="float: right;"><h1><a style="text-decoration: none;" href="../blog_post/all_posts.php">Blogs</a></h1></button>
            <h1 style="text-align: center;">Welcome Admin. You are the woner of this website.</h1>
          <?php
          if (!$admin->getSession()){
              echo 'Please login';
              
          }  else {
              header("location: all_posts.php");
//              ?>
            
            //<?php
//              echo $admin_name;
          }
          
          ?>
             <form action="" method="post">
                <table style="line-height: 40px; color: white; margin-left: 33%; background-color: gray;" class="table leftalign">
                    <tr>
                        <td>User Name</td>
                        <td><input type="text" name="name" required/></td>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td><input type="password" name="password" required/></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" value="login"></td>
                    </tr>
                </table>
            </form>
        </div>
        <?php include("../../../footer.php");?>
    </body>
</html>

