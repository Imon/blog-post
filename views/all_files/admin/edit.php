<?php
session_start();
use App\controller\controller_class\blog;
use App\controller\controller_class\details;
use App\controller\controller_class\helper;

include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."PhpProject1".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");
$username = $_SESSION['admin_name'];
$helper = new helper();
$update = new details();
$post = new blog;
$single_post = $post->singlePost($_GET['id']);
$id = $_GET['id'];
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>My Blogs</title>
        <link href="../../../style.css" rel="stylesheet" type="text/css"/>
    </head>
    
    
    <body>
        
        <!--Header Area-->
        <?php include("../../../header.php");?>
        <!--end header area-->
        
        <div class="area">
            
            <div class="post_body">
                <h1 style="text-align: center; "><?php echo "Hello,"." ".$username; ?> | <a style="text-decoration: none; color: #fff;" href="admin_logout.php">Logout</a> | <a style="text-decoration: none; color: #fff;" href="../../../calculator.php">Calculator</a>|<a style="text-decoration: none; color: #fff;" href="all_posts.php">Back</a></h1><br><br>
<!--                <hr>
                <h1><?php // echo $single_post['title']; ?></h1>
                <hr>
                <p>
                    <?php //echo "Writer : ".$single_post['author'].", "; echo "Date : ".$helper->dateFormat($single_post['date']);?>
                </p>
                    <hr>
                    <br>
                    <?php //echo $single_post['body'];?>
                <hr>
                -->
                <form action="update.php" method="post">
                <table style="line-height: 40px; color: white; background-color: gray;">
                    <input type="hidden" name="id" value="<?php echo $single_post['id'];?>">
                            <tr>
                                <td>Title</td>
                                <td><input type="text" name="title" value="<?php echo $single_post['title']; ?>"/></td>
                            </tr>
                            <tr>
                                <td>Description</td>
                                <td><textarea class="" name="body" value="<?php echo $single_post['id'];?>"><?php echo $single_post['body'];?></textarea></td>
                            </tr>
                           
                            <tr>
                                <td>Author</td>
                                <td><input type="text" name="author" value="<?php echo $single_post['author'];?>"/></td>
                            </tr>
                            <tr>
                                <td>Tags</td>
                                <td><input type="text" name="tags" value="<?php echo $single_post['tags'];?>"/></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><button value="<?php echo $single_post['id'];?>">Submit</button></td>
                            </tr>
                    </table>
            </form>
            </div>
            <?php include("../blog_post/sidebar.php");?>
        </div>
        
        <!--Footer Area-->
        <?php include("../../../footer.php");?>
        <!--End of Footer area-->
    </body>
</html>

