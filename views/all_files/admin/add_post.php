<?php
session_start();
use App\controller\controller_class\Admin;
use App\controller\controller_class\blog;
include($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."PhpProject1".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");

$admin = new Admin();
$logedIn = $admin->getSession();
if(!$logedIn){
    header("location: admin_login.php");
}else{
    $name = $_SESSION['admin_name'];
}
$blog = new blog();
$category = $blog->sidebar(); //sidebar function is for all category


//submitt post
if($_SERVER["REQUEST_METHOD"] == "POST"){
    $category = $_POST['category'];
    $title = $_POST['title'];
    $body = $_POST['body'];
    $image = $_FILES['image'];
    $author = $_POST['author'];
    $tags = $_POST['tags'];
    
    
//    Upload image
    $image = $_FILES['image']['name'];
    $image_explode = explode(".",$image);
    $ext = array_pop($image_explode);
    $image_name = implode(".", $image_explode);
    $new_name = $image_name.time().".".$ext;
    
    
    
    $file_name = $_FILES['image']['tmp_name'];
    
    $destination_folder = $_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."PhpProject1".DIRECTORY_SEPARATOR."image".DIRECTORY_SEPARATOR;
//    End of Image upload criteria
    if(empty($category) || empty($title) || empty($body) || empty($image) || empty($author) || empty($tags)){
        echo "<script>alert('Any field cannot be empty!!!!')</script>";
    }elseif(move_uploaded_file($file_name, $destination_folder.$new_name)){
//        var_dump($tags);
//        die();
        $blog->store($category, $title, $body, $new_name, $author, $tags); //or die("Cannot store!!!!")
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Registration Form</title>
        <link href="../../../style.css" rel="stylesheet" type="text/css"/>
    </head>
    
    
    <body class="body">
        <?php include("../../../header.php");?>
        
        <div class="area">
            
            <div style="margin-left: 33%;">
                <?php echo $name;?>
                <button><a href="all_posts.php">Blog</a></button>
                <button><a href="add_category.php">Add Category</a></button>
                <button><a href="admin_logout.php">Logout</a></button><br><br>
            </div>

            
            <form action="" method="post" enctype="multipart/form-data">
                <table style="line-height: 40px; color: white; margin-left: 33%; background-color: gray;">
                            <tr>
                                <td>Category</td>
                                <td>
                                    <select id="category" name="category">
                                        <option>Select Category</option>
                                            <?php
                                                foreach ($category as $all_category){ ?>
                                        <option value="<?php echo $all_category['id']?>">
                                            <?php echo $all_category['name']; } ?>
                                        </option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Title</td>
                                <td><input type="text" name="title"/></td>
                            </tr>
                            <tr>
                                <td>Description</td>
                                <td><textarea class="" name="body"></textarea></td>
                            </tr>
                            <tr>
                                <td>Image</td>
                                <td><input type="file" name="image"/></td>
                            </tr>
                            <tr>
                                <td>Author</td>
                                <td><input type="text" name="author"/></td>
                            </tr>
                            <tr>
                                <td>Tags</td>
                                <td><input type="text" name="tags"/></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><input type="submit" name="submit" value="Submit"/></td>
                            </tr>
                    </table>
            </form>
        </div>
        
        <?php include("../../../footer.php");?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="js/custom.js"></script>
        <script src="js/parallax.js"></script>
    </body>
</html>


