<?php
session_start();
use App\controller\controller_class\details;
include_once ($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."PhpProject1".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");

//include_once '../../../src/controller/controller_class/details.php';

$user = new details();
if($user->getSession()){
    header("location: show.php");
    die();
}

if($_SERVER["REQUEST_METHOD"] == "POST"){
    $email = $_POST['email'];
    $password = $_POST['password'];
    if(!empty($email) && !empty($password)){
        $login = $user->login($email, $password);
        if ($login){
            header("location: show.php");
        }
        else {
        echo "<script>alert('email or password not matched!')</script>";
    }
    }  else {
        echo "<script>alert('Field cannot be empty!')</script>";;
    }  
}

?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Your Details</title>
        <link href="../../../style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php include("../../../header.php");?>
        <div class="area">
            <button style="float: right;"><h1><a style="text-decoration: none;" href="../../../index.php">Sign Up</a></h1></button>
            <button style="float: right;"><h1><a style="text-decoration: none;" href="../blog_post/all_posts.php">Blogs</a></h1></button>
            
            <form action="" method="post">
                <table class="table leftalign">
                    <tr>
                        <td>E-mail</td>
                        <td><input type="email" name="email" required/></td>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td><input type="password" name="password" required/></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" value="login"></td>
                    </tr>
                </table>
            </form>
        </div>
        <?php include("../../../footer.php");?>
    </body>
</html>

