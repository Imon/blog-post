<?php
use App\controller\controller_class\details;
include_once ($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."PhpProject1".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");

//require_once '../../../src/controller/controller_class/details.php';


$details = new details();

$your_info = $details->recycle();
//echo $_POST['first_name']; die();

?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Your Details</title>
        <link href="../../../style.css" rel="stylesheet" type="text/css"/>
    </head>
    
    
    <body>
        <?php include("../../../header.php");?>
        <div class="area">
            
             <table border="1" align="center">
            <tr>
                <td>Sl. No</td>
                <td>Name</td>
                <td>Father Name</td>
                <td>Mother Name</td>
                <td>E-Mail</td>
                <td>Date of Birth</td>
                <td colspan="2">Action</td>
            </tr>
            <?php
            $i=0;
            foreach ($your_info as $info){
                
                $i++;
            ?>
            
            
            <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $info['first_name']." ".$info['last_name']; ?></td>
                <td><?php echo $info['father_name']; ?></td>
                <td><?php echo $info['mother_name']; ?></td>
                <td><?php echo $info['email']; ?></td>
                <td><?php echo $info['d_o_birth']; ?></td>
                <td>
                    <form action="recover.php" method="get">
                        <input type="hidden" name="id" value="<?php echo $info['id']?>"/>
                        <button type="submit">Recover</button>
                    </form>
                </td>
                <td>
                    <form action="show.php" method="get">
                        <input type="hidden" name="id" value="<?php echo $info['id']?>"/>
                        <button type="submit">Back To Home</button>
                    </form>
                    
                </td>
               
            </tr>
            <?php
                };
            ?>
        </table>
        <h1 style="text-align: center"><a style="text-decoration: none;" href="show.php">Back To Home</a></h1>
        </div>
        <?php include("../../../footer.php");?>
    </body>
</html>

