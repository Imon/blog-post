<?php
session_start();

use App\controller\controller_class\details;

include_once ($_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "PhpProject1" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php");


$user = new details();
$username = $_SESSION['user_name'];
$uid = $_SESSION['uid'];

if (!$user->getSession()) {
    header("location: login.php");
    die();
}
$profil = $user->profile();
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Your Details</title>
        <link href="../../../style.css" rel="stylesheet" type="text/css"/>
        <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    </head>


    <body>
        <?php include("../../../header.php"); ?>
        <div class="area">
            <h1 style="text-align: center;"><?php echo "Hello," . " " . $username; ?> <a style="text-decoration: none; color: #fff;" href="logout.php">Logout</a> | See all <a style="text-decoration: none; color: #fff;" href="show.php">Profile</a> | <a style="text-decoration: none; color: #fff;" href="../../../calculator.php">Calculator</a></h1>
            <div class="container">
                <div class="col-sm-10 col-sm-offset-1">
                    <div class="row">
                        <table class="table table-bordered">
                            <tr style="text-align: center; font-weight: bold; font-size: 25px;">

                                <td>Name</td>
                                <td>Father Name</td>
                                <td>Mother Name</td>
                                <td>E-Mail</td>
                                <td>Date of Birth</td>
                                <td colspan="4">action</td>

                            </tr>
                            <?php foreach ($profil as $profile) { ?>
                                <tr>
                                    <td><?php echo $profile['first_name'] . " " . $profile['last_name']; ?></td>
                                    <td><?php echo $profile['father_name']; ?></td>
                                    <td><?php echo $profile['mother_name']; ?></td>
                                    <td><?php echo $profile['email']; ?></td>
                                    <td><?php echo $profile['d_o_birth']; ?></td>
                                    <td>
                                        <form action="edit.php" method="post">
                                            <input type="hidden" name="id" value="<?php echo $profile['id'] ?>"/>
                                            <button style="color: black;" type="submit">Edit</button>
                                        </form>
                                    </td>
                                    <td>
                                        <form action="delete.php" method="post">
                                            <input type="hidden" name="id" value="<?php echo $profile['id'] ?>"/>
                                            <button style="color: black;" type="submit">Delete</button>
                                        </form>
                                    </td>
                                    <td>
                                        <form action="trash.php" method="post">
                                            <input type="hidden" name="id" value="<?php echo $profile['id'] ?>"/>
                                            <button style="color: black;" type="submit">Trash</button>
                                        </form>
                                    </td>
                                    <td>
                                        <form action="recycle.php" method="post">
                                            <input type="hidden" name="id" value="<?php echo $profile['id'] ?>"/>
                                            <button style="color: black;" type="submit">Recycle Bin</button>
                                        </form>
                                    </td>
                                </tr>
                                <img src="../../../image/<?php echo $profile['image']; ?>" alt="" width="150px" height="160px">
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>