 <?php
 use App\controller\controller_class\details;
 include_once ($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."PhpProject1".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");

    
    $detail = new details();
    //echo $_GET['id'];    die();
    $id = $_POST['id'];
    $details = $detail->edit($id);
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link href="../../../style.css" rel="stylesheet" type="text/css"/>
    </head>
    
    
    <body>
        <?php include("../../../header.php");?>
        <div class="area">
            <form action="update.php" method="post" class="table">
                
                <table>
                    <tr>
                        <td><input type="hidden" name="id" value="<?php echo $details['id'];?>"></td>
                    </tr>
                    <tr>
                        <td>First Name</td>
                        <td><input type="text" name="first_name" value="<?php echo $details['first_name'];?>"></td>
                    </tr>
                    <tr>
                        <td>Last Name</td>
                        <td><input type="text" name="last_name" value="<?php echo $details['last_name'];?>"></td>
                    </tr>
                    <tr>
                        <td>Father Name</td>
                        <td><input type="text" name="father_name" value="<?php echo $details['father_name'];?>"></td>
                    </tr>
                    <tr>
                        <td>Mother Name</td>
                        <td><input type="text" name="mother_name" value="<?php echo $details['mother_name'];?>"></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td><input type="text" name="email" value="<?php echo $details['email'];?>"></td>
                    </tr>
                    <tr>
                        <td>Date of Birth</td>
                        <td><input type="date" name="d_o_birth" value="<?php echo $details['d_o_birth'];?>"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><button type="submit">Update</button></td>
                    </tr>
                </table>
            </form> 
        </div> 
        <?php include("../../../footer.php");?>
    </body>
</html>
        


