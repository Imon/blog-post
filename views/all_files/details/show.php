<?php
session_start();

use App\controller\controller_class\details;

include_once ($_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "PhpProject1" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php");

//require_once '../../../src/controller/controller_class/details.php';


$details = new details();
if (!$details->getSession()) {
    header("location:login.php");
}
$username = $_SESSION['user_name'];

$your_info = $details->index();
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>All Profile</title>
        <link href="../../../style.css" rel="stylesheet" type="text/css"/>
        <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    </head>


    <body>
        <?php include("../../../header.php"); ?>
        <div class="area">
            <h1 style="text-align: center; "><?php echo "Hello," . " "; ?><a style="text-decoration: none; color: #fff;" href="profile.php"><?php echo $username ?></a> | <a style="text-decoration: none; color: #fff;" href="logout.php">Logout</a> | <a style="text-decoration: none; color: #fff;" href="../../../calculator.php">Calculator</a>
                | <a style="text-decoration: none; color: #fff;" href="../blog_post/all_posts.php">Blogs</a>
            </h1>


            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="">
                            <table class="table table-bordered">
                                <tr style="text-align: center; font-weight: bold; font-size: 25px;">
                                    <td>Sl. No</td>
                                    <td>Name</td>
                                    <td>Father Name</td>
                                    <td>Mother Name</td>
                                    <td>E-Mail</td>
                                    <td>Date of Birth</td>
                                    <td>Image</td>

                                </tr>
                                <?php
                                $i = 0;
                                foreach ($your_info as $info) {
                                    $i++;
                                    ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $info['first_name'] . " " . $info['last_name']; ?></td>
                                        <td><?php echo $info['father_name']; ?></td>
                                        <td><?php echo $info['mother_name']; ?></td>
                                        <td><?php echo $info['email']; ?></td>
                                        <td><?php echo $info['d_o_birth']; ?></td>
                                        <td><img src="../../../image/<?php echo $info['image']; ?>" width="50px" height="50px"/></td>

                                    </tr>
                                    <?php
                                };
                                ?>
                            </table>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
        <?php include("../../../footer.php"); ?>



        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>


