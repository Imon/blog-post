<?php
    use App\controller\controller_class\blog;
    use App\controller\controller_class\details;
    use App\controller\controller_class\helper;
    use App\controller\controller_class\Uploader;
    include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."PhpProject1".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");
    
    $_all_catagory = new blog();
    $catagory = $_all_catagory->sidebar();
    $all_posts = $_all_catagory->select_for_sidebar();
    $helper = new helper();
    
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>My Blog</title>
        <link href="../../../style.css" rel="stylesheet" type="text/css"/>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>

    <body>
        <div class="float">
            <div class="sidebar">
                <div class="categories">
                    <div class="title_header_background">
                        <h2>Categories</h2>
                    </div>
                    
                    <ul style="list-style: none">
                        <?php
                        foreach ($catagory as $every_catagory){
                        ?>
                        <li><a href="cat_post.php?categorys=<?php echo $every_catagory['id']; ?>"><?php echo $every_catagory['name']; ?></a></li>
                        <?php }?>
                    </ul>
                </div>
                <div class="article">
                    <h2 class="title_header_background">Latest Article</h2>
                    <?php
                        foreach ($all_posts as $every_article){
                        ?>
                    <ul style="list-style: none">
                       <li><a style="text-decoration: none; color: red;" href="single_post.php?id=<?php echo $every_article['id']; ?>"><?php echo $every_article['title']; ?></a></li>
                                         
                    </ul>
                    <img src="../../../image/<?php echo $every_article['image'];?>" alt="" width="40px" height="40px"/>
                        <?php echo $helper->textShorten($every_article['body'],60);?>
                    <a style="text-decoration: none; color: red;" href="single_post.php?id=<?php echo $every_article['id']?>">Read More</a>
                    
                        <?php  echo '<br>'; echo '<br>'; }?>
                </div>
                <?php
                $counter_name = "counter.txt";
// Check if a text file exists. If not create one and initialize it to zero.
if (!file_exists($counter_name)) {
  $f = fopen($counter_name, "w");
  fwrite($f,"0");
  fclose($f);
}
// Read the current value of our counter file
$f = fopen($counter_name,"r");
$counterVal = fread($f, filesize($counter_name));
fclose($f);
// Has visitor been counted in this session?
// If not, increase counter value by one
if(!isset($_SESSION['hasVisited'])){
  $_SESSION['hasVisited']="yes";
  $counterVal++;
  $f = fopen($counter_name, "w");
  fwrite($f, $counterVal);
  fclose($f); 
}
echo "You are visitor number $counterVal to this site";
?>
                <!--
                      PeoplePerHour Profile Widget
                      The div#pph-hire me is the element
                      where the iframe will be inserted.
                      You may move this element wherever
                      you need to display the widget
                -->
                  <div style="float: left; position: relative;">
                     <div id="pph-hireme"></div>
                        <script type="text/javascript">
                                (function(d, s) {
                                    var useSSL = 'https:' == document.location.protocol;
                                    var js, where = d.getElementsByTagName(s)[0],
                                    js = d.createElement(s);
                                    js.src = (useSSL ? 'https:' : 'http:') +  '//www.peopleperhour.com/hire/2382581226/1451343.js?width=200&height=110&orientation=vertical&theme=dark&rnd='+parseInt(Math.random()*10000, 10);
                                    try { where.parentNode.insertBefore(js, where); } catch (e) { if (typeof console !== 'undefined' && console.log && e.stack) { console.log(e.stack); } }
                                }(document, 'script'));
                        </script> 
                  </div>

            </div>
        </div>
        
    </body>
</html>
