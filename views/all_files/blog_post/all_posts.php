<?php

session_start();

use App\controller\controller_class\Admin;
use App\controller\controller_class\blog;
use App\controller\controller_class\details;
use App\controller\controller_class\helper;

include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."PhpProject1".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");




$admin = new Admin();
$all_posts_class = new blog;
$helper = new helper();
$details = new details();
//$all_posts = $all_posts_class->select();
if($admin->getSession()){
    $user_id = $_SESSION['uid'];
    if(isset($_POST['liked'])){
        $post_id = $_POST['post_id'];
        $query = mysql_query("SELECT * FROM blog WHERE id = '$post_id'") or die("Error---1");
        $row = mysql_fetch_assoc($query);
        $n = $row['likes'];

        mysql_query("UPDATE blog SET likes = $n+1 WHERE id = $post_id") or die("Error---2");
        mysql_query("INSERT INTO likes(id, user_id, post_id) VALUES (null, '$user_id', '$post_id')") or die("Error---3");
        die();
    }
    if(isset($_POST['unliked'])){
        $post_id = $_POST['post_id'];
        $query = mysql_query("SELECT * FROM blog WHERE id = '$post_id'") or die("Error---1");
        $row = mysql_fetch_assoc($query);
        $n = $row['likes'];

        mysql_query("UPDATE blog SET likes = $n-1 WHERE id = $post_id") or die("Error---2");
        mysql_query("DELETE FROM likes WHERE user_id = '$user_id' AND post_id = ".$post_id."") or die("Error---3");
        die();
    }
}
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>My Blogs</title>
        <link href="../../../style.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../../../font_awesome/css/font-awesome.min.css"/>
<!--        <script src="../../../js/jquery-3.1.1.js"></script>-->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.1.1.min.js"></script>
        
    </head>
    <body>
        
        <!--Header Area-->
        <?php include("../../../header.php");?>
        <!--end header area-->
        
        <div class="area">
            <div class="post_body">
                <?php
                    if($admin->getSession()){
                        $username = $_SESSION['user_name'];
                    ?>
                <h3 style="text-align: center;">
                    <?php echo "Hello,"." ".$username;?> | 
                    <a style="text-decoration: none; color: #fff;" href="../details/show.php">All Profile</a> | 
                    <a style="text-decoration: none; color: #fff;" href="../details/logout.php">Logout</a> |
                    <a style="text-decoration: none; color: #fff;" href="../../../calculator.php">Calculator</a>
                </h3>
                    <?php } else{ ?>
                <h1 style="text-align: center;"> 
                    <a style="text-decoration: none; color: #fff;" href="../details/login.php">Please Login</a> | 
                    <a style="text-decoration: none; color: #fff;" href="../../../calculator.php">Calculator</a>                    
                </h1><?php }?><br><br>
               
                <!--pagination-->
               <?php
                $post_per_page = 3;
                 //$number_of_page = $all_posts_class->pagination();
                 if(isset($_GET['page'])){
                    $page = $_GET['page'];
                 }  else {
                     $page = 1;
                 }
                 $start_from = ($page-1) * $post_per_page;

              
            
            $pagignation = $all_posts_class->pagination();
                $total_page = ceil($pagignation/$post_per_page);
                for($i =1; $i<=$total_page; $i++){
                     echo "<span class='pagination'><a href='all_posts.php?page=$i'>".$i." |</a></span>";
                }
                //Pagignation end
                ?>
                <!--end of pagination-->
                <?php 
                $result = $all_posts_class->select($start_from, $post_per_page);
                //Foreach start
                foreach ($result as $rows){
                ?>
                
                <!--all posts-->
                    <hr>
                    <?php echo "<br>"; echo "<br>"; ?>
                    
                        <h1><a style="text-decoration: none; color: #b98966;" href="single_post.php?id=<?php echo $rows['id']?>"><?php echo $rows['title'];?></a></h1>
                        <hr>
                        <?php
                            echo "Writer : ".$rows['author'].",  ";
                            echo "Date : ".$helper->dateFormat($rows['date']);
                            ?>
                            <hr>
                            <?php
                            echo "<br>";
                            echo $helper->textShorten($rows['body']);
                            ?>
                            <a style="text-decoration: none; color: red;" href="single_post.php?id=<?php echo $rows['id']?>">Read More</a>
                            <br>
                            <br>

                            <!--Like/Unlike-->
                            <div>
                                <hr>
                                    <?php echo $rows['likes']." "."<span style='font-size: 10px'>Likes</span>";?>
                                <hr>
                                <?php
                                if($admin->getSession()){
                                    $user_id = $_SESSION['uid'];
                                     $query = mysql_query("SELECT * FROM likes WHERE user_id = '$user_id' AND post_id = ".$rows['id']."");
                                    //$all_posts_class->like($user_id, $post_id);
                                 if(mysql_num_rows($query) == 1 ){ ?>
                                <form>
                                <a class="unlike" href="" id="<?php echo $rows['id'];?>"><img src="../../../image/unlike.png" width="20px" height="20px"><span style="color: #fff; text-decoration: none; padding: 0 4px;">Unlike</span></a>
                                    <?php }  else { ?>
                                     <a class="like" href="" id="<?php echo $rows['id'];?>"><img src="../../../image/Like.png" width="20px" height="20px"><span style="color: #fff; text-decoration: none; padding: 0 4px;">Like</span></a>
                                <?php } } else {?>
                                     <a class="like" href="../details/login.php" id="<?php echo $rows['id'];?>"><img src="../../../image/Like.png" width="20px" height="20px"><span style="color: #fff; text-decoration: none; padding: 0 4px;">Like</span></a>
                                <?php } ?>
                                         <i class="fa fa-comment" aria-hidden="true">
                                             <a id="comment">
                                                <span id="<?php echo $rows['id'];?>" style="cursor: pointer; color: #fff; text-decoration: none; padding: 0 4px;">
                                                    Comments
                                                </span>
                                             </a>
                                         </i>
                                         <textarea style="resize: none;" class="comments" type="comment" rows="1" cols="20"></textarea>
                                        <i class="fa fa-share" aria-hidden="true"><a href=""><span style="color: #fff; text-decoration: none; padding: 0 4px;">Share</span></a></i>
                                    </form>
                                    <hr>
                            </div>
                    <?php
                        echo "<br>";
                        echo "<br>";
                        echo "<br>";
                    ?>
                   <hr>

                <?php 
                }
                //End Foreach
                ?>
            </div>
           
            <?php include("sidebar.php"); ?>
        </div>
        <!--Footer Area-->
        <?php include("../../../footer.php");?>
        <!--End of Footer area-->
        <script type="text/javascript">
               $(document).ready(function(){
                   $('.like').click(function(){
                       var post_id = $(this).attr('id');
                       $.ajax({
                           url: 'all_posts.php',
                           type: 'post',
                           async: false,
                           data: {
                               'liked': 1,
                               'post_id': post_id
                           },
                           success: function(){
                               
                           }
                       });
                   });
                   $('.unlike').click(function(){
                       var post_id = $(this).attr('id');
                       $.ajax({
                           url: 'all_posts.php',
                           type: 'post',
                           async: false,
                           data: {
                               'unliked': -1,
                               'post_id': post_id
                           },
                           success: function(){
                               
                           }
                       });
                   });
                   $("textarea").hide(function(){
                       $("#comment").click(function(){
                       $("textarea").toggle();
                    });
                  });
                
               });
        </script>
    </body>
</html>
