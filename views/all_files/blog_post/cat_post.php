<?php
session_start();

use App\controller\controller_class\Admin;
use App\controller\controller_class\blog;
use App\controller\controller_class\details;
use App\controller\controller_class\helper;
use App\controller\controller_class\Uploader;

include_once($_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "PhpProject1" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php");

$admin = new Admin();
$_all_catagory = new blog();
$helper = new helper();


if ($admin->getSession()) {
    $username = $_SESSION['user_name'];
    $user_id = $_SESSION['uid'];
    if (isset($_POST['liked'])) {
        $post_id = $_POST['post_id'];
        $query = mysql_query("SELECT * FROM blog WHERE id = '$post_id'") or die("Error---1");
        $row = mysql_fetch_assoc($query);
        $n = $row['likes'];

        mysql_query("UPDATE blog SET likes = $n+1 WHERE id = $post_id") or die("Error---2");
        mysql_query("INSERT INTO likes(id, user_id, post_id) VALUES (null, '$user_id', '$post_id')") or die("Error---3");
        die();
    }
    if (isset($_POST['unliked'])) {
        $post_id = $_POST['post_id'];
        $query = mysql_query("SELECT * FROM blog WHERE id = '$post_id'") or die("Error---1");
        $row = mysql_fetch_assoc($query);
        $n = $row['likes'];

        mysql_query("UPDATE blog SET likes = $n-1 WHERE id = $post_id") or die("Error---2");
        mysql_query("DELETE FROM likes WHERE user_id = '$user_id' AND post_id = " . $post_id . "") or die("Error---3");
        die();
    }
}
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>My Blog</title>
        <link href="../../../style.css" rel="stylesheet" type="text/css"/>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.1.1.min.js"></script>
    </head>
    <body>

        <!--Header Area-->
        <?php include("../../../header.php"); ?>
        <!--end header area-->

        <div class="area">
            <div class="post_body">
                <h2 style="text-align: center; ">
                    <?php
                    if($admin->getSession()){
                        $username = $_SESSION['user_name'];
                        echo "Hello," . " " . $username;
                    ?>|<a style="text-decoration: none; color: #fff;" href="logout.php">Logout</a> |
                        <?php }  else { ?>
                    <a style="text-decoration: none; color: #fff;" href="../details/login.php">Please Login</a>|
                <?php }?>
                    <a style="text-decoration: none; color: #fff;" href="all_posts.php">Back</a>|<a style="text-decoration: none; color: #fff;" href="../../../calculator.php">Calculator</a></h2><br><br>

                <?php
                if (isset($_GET['categorys']) && $_GET['categorys'] == !NULL) {
                    $id = $_GET['categorys'];
                    $query = mysql_query("SELECT * FROM `blog` WHERE `category` = '$id'");
                    while ($rows = mysql_fetch_assoc($query)) {
                        ?>
                        <hr>
                        <?php echo "<br>";
                        echo "<br>";
                        ?>
                        <h1><a style="text-decoration: none; color: red;" href="single_post.php?id=<?php echo $rows['id']; ?>"><?php echo $rows['title']; ?></a></h1>
                        <h3>
                            <hr>
                            <?php
                            echo "Writer : " . $rows['author'] . ",  ";
                            echo $helper->dateFormat($rows['date']);
                            ?>
                            <hr>
                        <?php echo "<br>"; ?>
                        </h3>
                        <?php echo $helper->textShorten($rows['body']); ?>
                        <a style="text-decoration: none; color: red;" href="single_post.php?id=<?php echo $rows['id']; ?>">Read More</a>
                        <?php echo "<br>";
                        echo "<br>";
                        ?>
                        <hr>
                        <?php echo $rows['likes'] . " " . "<span style='font-size: 10px'>Likes</span>"; ?>
                        <hr>
                        <?php
                        if ($admin->getSession()) {
                            $user_id = $_SESSION['uid'];
                            $result = mysql_query("SELECT * FROM likes WHERE user_id = $user_id AND post_id = " . $rows['id'] . "") or die("Problem in likes!!");
                            if (mysql_num_rows($result) == 1) {
                                ?>
                                <a class="unlike" href="" id="<?php echo $rows['id']; ?>"><img src="../../../image/unlike.png" width="20px" height="20px"><span style="color: #fff; text-decoration: none; padding: 0 4px;">Unlike</span></a>
                            <?php } else { ?>
                                <a class="like" href="" id="<?php echo $rows['id']; ?>"><img src="../../../image/Like.png" width="20px" height="20px"><span style="color: #fff; text-decoration: none; padding: 0 4px;">Like</span></a>
                            <?php }
                        } else {
                            ?>
                            <a class="like" href="../details/login.php" id="<?php echo $rows['id']; ?>"><img src="../../../image/Like.png" width="20px" height="20px"><span style="color: #fff; text-decoration: none; padding: 0 4px;">Like</span></a>
        <?php } ?>
                        <i class="fa fa-comment" aria-hidden="true"><a href=""><span style="color: #fff; text-decoration: none; padding: 0 4px;">Comments</span></a></i>
                        <i class="fa fa-share" aria-hidden="true"><a href=""><span style="color: #fff; text-decoration: none; padding: 0 4px;">Share</span></a></i>
                        <hr>

                        <?php
                    }
                } else {
                    die("404 BAD REQUEST");
                }
                ?>
            </div>
        <?php include("sidebar.php"); ?>
        </div>
        <!--Footer Area-->
<?php include("../../../footer.php"); ?>
        <!--End of Footer area-->


        <!--Like/Unlike Start here with JQuery and Ajax -->

        <script type="text/javascript">
            $(document).ready(function () {
                $('.like').click(function () {
                    var post_id = $(this).attr('id');
                    $.ajax({
                        url: 'cat_post.php',
                        type: 'post',
                        async: false,
                        data: {
                            'liked': 1,
                            'post_id': post_id
                        },
                        success: function () {

                        }
                    });
                });
                $('.unlike').click(function () {
                    var post_id = $(this).attr('id');
                    $.ajax({
                        url: 'cat_post.php',
                        type: 'post',
                        async: false,
                        data: {
                            'unliked': -1,
                            'post_id': post_id
                        },
                        success: function () {

                        }
                    });
                });
            });
        </script>
    </body>
</html>


