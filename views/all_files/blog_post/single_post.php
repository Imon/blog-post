<?php
session_start();
use App\controller\controller_class\blog;
use App\controller\controller_class\details;
use App\controller\controller_class\helper;
use App\controller\controller_class\Admin;

include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."PhpProject1".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");
$username = $_SESSION['user_name'];

$admin = new Admin();
$helper = new helper();

$post = new blog;
$single_post = $post->singlePost($_GET['id']);
$id = $_GET['id'];

if(!$admin->getSession()){
    header("location:../details/login.php");
}else{
    $user_id = $_SESSION['uid'];
    if(isset($_POST['liked'])){
        $post_id = $_POST['post_id'];
        $query = mysql_query("SELECT * FROM blog WHERE id = '$post_id'") or die("Error---1");
        $row = mysql_fetch_assoc($query);
        $n = $row['likes'];

        mysql_query("UPDATE blog SET likes = $n+1 WHERE id = $post_id") or die("Error---2");
        mysql_query("INSERT INTO likes(id, user_id, post_id) VALUES (null, '$user_id', '$post_id')") or die("Error---3");
        die();
    }
    if(isset($_POST['unliked'])){
        $post_id = $_POST['post_id'];
        $query = mysql_query("SELECT * FROM blog WHERE id = '$post_id'") or die("Error---1");
        $row = mysql_fetch_assoc($query);
        $n = $row['likes'];

        mysql_query("UPDATE blog SET likes = $n-1 WHERE id = $post_id") or die("Error---2");
        mysql_query("DELETE FROM likes WHERE user_id = '$user_id' AND post_id = ".$post_id."") or die("Error---3");
        die();
    }
}
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>My Blogs</title>
        <link href="../../../style.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="../../../font_awesome/css/font-awesome.min.css">
        <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.1.1.min.js"></script>
    </head>
    
    
    <body>
        
        <!--Header Area-->
        <?php include("../../../header.php");?>
        <!--end header area-->
        
        <div class="area">
            
            <div class="post_body">
                <h1 style="text-align: center; "><?php echo "Hello,"." ";?><a style="text-decoration: none; color: #fff;" href="../details/show.php"><?php echo $username?></a> | <a style="text-decoration: none; color: #fff;" href="../details/logout.php">Logout</a> | <a style="text-decoration: none; color: #fff;" href="../../../calculator.php">Calculator</a>|<a style="text-decoration: none; color: #fff;" href="all_posts.php">Back</a></h1><br><br>
               <?php
                if(isset($id)){ ?>
                <hr>
                <h1><?php echo $single_post['title']; ?></h1>
                <hr>
                <p>
                    <?php echo "Writer : ".$single_post['author'].", "; echo "Date : ".$helper->dateFormat($single_post['date']);?>
                </p>
                    <hr>
                    <br>
                    <?php echo $single_post['body'];
                    echo '<br>';
                    ?>
                    <br>
                <hr>
                                                    
                    <!--Like/Unlike-->
                    
                        <div>
                            <?php echo $single_post['likes']." "."<span style='font-size: 10px'>Likes</span>";?>
                            <hr>
                            <?php
                            if($admin->getSession()){
                                $user_id = $_SESSION['uid'];
                                 $result = mysql_query("SELECT * FROM likes WHERE user_id = '$user_id' AND post_id = ".$single_post['id']."");
                                //$all_posts_class->like($user_id, $post_id);
                             if(mysql_num_rows($result) == 1 ){ ?>
                            <a class="unlike" href="" id="<?php echo $single_post['id'];?>"><img src="../../../image/unlike.png" width="20px" height="20px"><span style="color: #fff; text-decoration: none; padding: 0 4px;">Unlike</span></a>
                                <?php }  else { ?>
                                 <a class="like" href="" id="<?php echo $single_post['id'];?>"><img src="../../../image/Like.png" width="20px" height="20px"><span style="color: #fff; text-decoration: none; padding: 0 4px;">Like</span></a>
                            <?php } } else {?>
                                 <a class="like" href="../details/login.php" id="<?php echo $single_post['id'];?>"><img src="../../../image/Like.png" width="20px" height="20px"><span style="color: #fff; text-decoration: none; padding: 0 4px;">Like</span></a>
                            <?php } ?>
                                <i class="fa fa-comment" aria-hidden="true"><a href=""><span style="color: #fff; text-decoration: none; padding: 0 4px;">Comments</span></a></i>
                                <i class="fa fa-share" aria-hidden="true"><a href=""><span style="color: #fff; text-decoration: none; padding: 0 4px;">Share</span></a></i>
                            <hr>
                        </div>
                    <!--Like/Unlike-->
                <?php }  else {
                            die("404 Not Found!!!!! BAD REQUEST");
                        }?>
            </div>
            <?php include("sidebar.php");?>
        </div>
        
        <!--Footer Area-->
        <?php include("../../../footer.php");?>
        <!--End of Footer area-->
         <script type="text/javascript">
               $(document).ready(function(){
                   $('.like').click(function(){
                       var post_id = $(this).attr('id');
                       $.ajax({
                           url: 'single_post.php',
                           type: 'post',
                           async: false,
                           data: {
                               'liked': 1,
                               'post_id': post_id
                           },
                           success: function(){
                               
                           }
                       });
                   });
                   $('.unlike').click(function(){
                       var post_id = $(this).attr('id');
                       $.ajax({
                           url: 'single_post.php',
                           type: 'post',
                           async: false,
                           data: {
                               'unliked': -1,
                               'post_id': post_id
                           },
                           success: function(){
                               
                           }
                       });
                   });
               });
        </script>
    </body>
</html>

